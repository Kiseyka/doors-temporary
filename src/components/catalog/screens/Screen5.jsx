import React from 'react';
import styles from './screen.module.scss';

import photo1 from '../../../assets/catalog/hinged-closets/001.jpg';
import photo2 from '../../../assets/catalog/hinged-closets/002.jpg';
import photo3 from '../../../assets/catalog/hinged-closets/003.jpg';
import photo4 from '../../../assets/catalog/hinged-closets/004.jpg';

const Screen5 = () => {
  return (
    <div className={styles.wrapper}>
      <h4 className={styles.title}>РАСПАШНЫЕ ШКАФЫ</h4>
      <div className={styles.content}>
        <img className={styles.image} src={photo1} alt="img" />
        <div className="col-span-2 indent-6">
          <p className={styles.text}>
            Шкафы-купе в современных интерьерах смотрятся стильно и оригинально,
            однако шкафы с классическими распашными дверями не перестают быть
            популярными среди покупателей.
          </p>
          <p className={styles.text}>
            Современный шкаф с распашными дверями гармонично впишется в любой
            дизайн комнаты благодаря большому разнообразию моделей. Распашные
            шкафы имеют презентабельный вид, при этом являются удобными и
            вместительными.
          </p>
        </div>
        <div className={styles.inner}>
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo3}
            alt="img"
          />
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo4}
            alt="img"
          />
        </div>
        <div>
          <p className={styles.text}>
            В MEBEL-178 Вы можете заказать распашной шкаф в спальню, гостиную,
            детскую или прихожую по индивидуальному проекту. Наши опытные
            дизайнеры помогут Вам на любом этапе от выбора материала и цвета
            изделия до наполнения.
          </p>
          <p className={styles.text}>
            Быстро рассчитать распашной шкаф можно позвонив по телефону
            +7(911)997-82-57, а так же заполнив форму обратной связи на нашем
            сайте.
          </p>
        </div>
        <img className={styles.image} src={photo2} alt="img" />
      </div>
    </div>
  );
};

export default Screen5;
