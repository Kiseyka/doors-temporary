import React from 'react';
import styles from './screen.module.scss';

import photo1 from '../../../assets/catalog/attic-closets/001.jpg';
import photo2 from '../../../assets/catalog/attic-closets/002.jpg';
import photo3 from '../../../assets/catalog/attic-closets/003.jpg';
import photo4 from '../../../assets/catalog/attic-closets/004.jpg';

const Screen4 = () => {
  return (
    <div className={styles.wrapper}>
      <h4 className={styles.title}>МАНСАРДНЫЕ ШКАФЫ</h4>
      <div className={styles.content}>
        <img className={styles.image} src={photo1} alt="img" />
        <div className="col-span-2 indent-6">
          <p className={styles.text}>
            Есть загородный дом? Не знаешь как грамотно организовать
            пространство под скатом крыши чтоб с пользой использовать каждый
            сантиметр площади? Хватит это терпеть!{' '}
          </p>
          <p className={styles.text}>
            Мансардный шкаф - вот то, что Вам нужно! Такой шкаф можно вписать в
            самые разные нестандартные зоны, не теряя при этом практичности и
            функциональности.
          </p>
          <p className={styles.text}>
            В Mebel-178 Вы можете заказать мансардный шкаф по индивидуальным
            размерам, который идеально впишется под Вашу крышу. А наши опытные
            дизайнеры с радостью помогут на любом этапе выбора от материалов до
            внутреннего наполнения. Посмотрите какие прекрасные идеи можно
            реализовать, разместив шкаф в наклонной части под крышей.
          </p>
        </div>
        <div className={styles.inner}>
          <img
            className={styles.image__long + ' ' + styles.item}
            src={photo3}
            alt="img"
          />
          <img
            className={styles.image__long + ' ' + styles.item}
            src={photo4}
            alt="img"
          />
        </div>
        <img className={styles.image} src={photo2} alt="img" />
      </div>
    </div>
  );
};

export default Screen4;
