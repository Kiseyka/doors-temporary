import React from 'react';
import styles from './screen.module.scss';

import photo1 from '../../../assets/catalog/built-in-closets/001.jpg';
import photo2 from '../../../assets/catalog/built-in-closets/002.jpg';
import photo3 from '../../../assets/catalog/built-in-closets/003.jpg';
import photo4 from '../../../assets/catalog/built-in-closets/004.jpg';
import photo5 from '../../../assets/catalog/built-in-closets/005.jpg';

const Screen0 = () => {
  return (
    <div className={styles.wrapper}>
      <h4 className={styles.title}>ШКАФЫ-КУПЕ ВСТРОЕННЫЕ</h4>
      <div className={styles.content}>
        <img className={styles.image} src={photo5} alt="img" />
        <p className={styles.text}>
          Были времена, когда шкаф-купе считался предметом роскоши. Сегодня же
          это предмет мебели, который отлично вписывается в любой интерьер. Наша
          компания изготавливает шкафы-купе из материалов, имеющих отличное
          качество и долгий срок службы. Несомненными преимуществами шкафов-купе
          является экономия пространства, функциональность и привлекательный
          внешний вид. А это очень актуально в современной жизни. Шкафы-купе
          могут быть прямыми и угловыми. Встроенными и отдельно-стоящими.
          Шкафы-купе - незаменимая вещь как для дома, так и для офиса.
        </p>
        <div className={styles.inner}>
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo4}
            alt="img"
          />
          <p className={styles.text__small + ' ' + styles.item}>
            Встроенные шкафы-купе занимают минимум полезной площади и идеально
            вписываются в отведенную под мебель зону. Встроенный шкаф-купе чаще
            всего устанавливают в нише, или вплотную к стенам. В таких шкафах
            нет стенок, вся конструкция шкафа крепится к стенкам, потолку и
            полу.
          </p>
        </div>
        <img className={styles.image} src={photo1} alt="img" />
        <div className={styles.inner}>
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo2}
            alt="img"
          />
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo3}
            alt="img"
          />
        </div>
      </div>
    </div>
  );
};

export default Screen0;
