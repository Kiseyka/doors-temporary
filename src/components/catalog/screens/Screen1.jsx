import React from 'react';

import styles from './screen.module.scss';
import photo1 from '../../../assets/catalog/interrior-walls/001.jpeg';
import photo2 from '../../../assets/catalog/interrior-walls/002.jpeg';
import photo3 from '../../../assets/catalog/interrior-walls/003.jpeg';
import photo4 from '../../../assets/catalog/interrior-walls/004.jpeg';

const Screen1 = () => {
  return (
    <div className={styles.Wrapper}>
      <h4 className={styles.title}>ПЕРЕГОРОДКИ МЕЖКОМНАТНЫЕ</h4>
      <div className={styles.content}>
        <img className={styles.image} src={photo1} alt="img" />
        <p className={styles.text}>
          Основная функция межкомнатных перегородок это разграничение площади
          помещения. Проще говоря - зонирования. Раздвижные межкомнатные
          перегородки это отличное решение, а так же простой и легкий способ
          деления открытого пространства. Они добавляют эстетические и
          художественные акценты в обстановку. Такие перегородки служат
          одновременно и стенами, и дверями.
        </p>
        <div className={styles.inner}>
          <img
            className={styles.image__long + ' ' + styles.item}
            src={photo4}
            alt="img"
          />
          <img
            className={styles.image__long + ' ' + styles.item}
            src={photo2}
            alt="img"
          />
        </div>
        <img className={styles.image} src={photo3} alt="img" />
      </div>
    </div>
  );
};

export default Screen1;
