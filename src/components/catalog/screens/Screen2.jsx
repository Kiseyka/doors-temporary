import React from 'react';

import styles from './screen.module.scss';
import photo1 from '../../../assets/catalog/freestanding-closets/001.jpeg';
import photo2 from '../../../assets/catalog/freestanding-closets/002.jpeg';
import photo3 from '../../../assets/catalog/freestanding-closets/003.jpeg';
import photo4 from '../../../assets/catalog/freestanding-closets/004.jpeg';

const Screen2 = () => {
  return (
    <div className={styles.wrpapper}>
      <h4 className={styles.title}>ШКАФЫ-КУПЕ ОТДЕЛЬНО СТОЯЩИЕ</h4>
      <div className={styles.content}>
        <img className={styles.image} src={photo2} alt="img" />
        <div className={styles.inner}>
          <p className={styles.text__small + ' ' + styles.item}>
            Это полноценный шкаф состоящий из корпуса(задняя стенка, боковые
            стенки, цоколь) и дверей-купе. Такой шкаф при необходимости можно
            легко переставить в другое место. Мы изготавливаем шкафы-купе из
            качественных и современных материалов, с хорошими комплектующими и
            фурнитурой. Такие шкафы можно эксплуатировать очень долго. Поэтому
            следует заранее решить, какой дизайн шкафа выбрать. А наши менеджеры
            с радостью Вам в этом помогут.
          </p>
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo3}
            alt="img"
          />
        </div>
        <div className={styles.inner}>
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo1}
            alt="img"
          />
          <p className={styles.text__small + ' ' + styles.item}>
            Наша компания предлагает огромный выбор фасадов и внутреннего
            наполнения для гардеробных и шкафов-купе. Для того, чтобы каждый наш
            клиент смог воплотить в реальность свою дизайнерскую задумку.
            Варианты исполнения дверей можно посмотреть в наших работах.
          </p>
        </div>
        <img className={styles.image} src={photo4} alt="img" />
      </div>
    </div>
  );
};

export default Screen2;
