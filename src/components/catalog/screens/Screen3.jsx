import React from 'react';
import styles from './screen.module.scss';

import photo1 from '../../../assets/catalog/compartment-doors/001.jpg';
import photo2 from '../../../assets/catalog/compartment-doors/002.jpg';
import photo3 from '../../../assets/catalog/compartment-doors/003.jpg';
import photo4 from '../../../assets/catalog/compartment-doors/004.jpg';
import photo5 from '../../../assets/catalog/compartment-doors/005.jpg';
import photo6 from '../../../assets/catalog/compartment-doors/006.jpg';

const Screen3 = () => {
  return (
    <div className={styles.wrpapper}>
      <h4 className={styles.title}>ДВЕРИ-КУПЕ</h4>
      <div className={styles.content}>
        <img className={styles.image} src={photo6} alt="img" />
        <div className={styles.inner}>
          <p className={styles.text + ' ' + styles.item}>
            В современной жизни сложно представить помещение, в котором не было
            бы дверей-купе. Ведь двери-купе это незаменимая вещь для закрытия
            ниш, гардеробных комнат и шкафов-купе. Наша компания изготавливает
            двери-купе любых размеров. Мы предлагаем огромный выбор качественных
            и современных материалов для изготовления дверей-купе. Варианты
            исполнения можно посмотреть на фото. А рассчитать стоимость быстро и
            очень просто можно воспользовавшись нашим калькулятором.
          </p>
          <img
            className={styles.image__small + ' ' + styles.item}
            src={photo4}
            alt="img"
          />
        </div>
        <img className={styles.image} src={photo3} alt="img" />
        <div className={styles.inner}>
          <img
            className={styles.image__long + ' ' + styles.item}
            src={photo2}
            alt="img"
          />
          <div className={styles.column + ' ' + styles.item}>
            <img className={styles.image__column} src={photo1} alt="img" />
            <img className={styles.image__column} src={photo5} alt="img" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Screen3;
