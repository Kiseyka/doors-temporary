'use client';
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ArrowIcon from '../../components/icons/ArrowIcon/ArrowIcon';
import {
  Screen0,
  Screen1,
  Screen2,
  Screen3,
  Screen4,
  Screen5,
} from './screens';

const buttons = [
  'ШКАФЫ-КУПЕ ВСТРОЕННЫЕ',
  'ПЕРЕГОРОДКИ МЕЖКОМНАТНЫЕ',
  'ШКАФЫ-КУПЕ ОТДЕЛЬНО СТОЯЩИЕ',
  'ДВЕРИ-КУПЕ',
  'МАНСАРДНЫЕ ШКАФЫ',
  'РАСПАШНЫЕ ШКАФЫ',
];

export default function Catalog() {
  const slider = React.useRef();
  const next = () => slider.current.slickNext();
  const previous = () => slider.current.slickPrev();
  const goTo = (index) => slider.current.slickGoTo(index);
  const settings = {
    dots: false,
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
  };
  const navButtons = buttons.map((element, index) => (
    <button className="button" key={index} onClick={() => goTo(index)}>
      {element}
    </button>
  ));

  return (
    <div className="catalog">
      <div className="top">
        <h3 className="title" id="top-catalog">
          КАТАЛОГ
        </h3>
        <nav className="buttons">{navButtons}</nav>
      </div>
      <div className="slider">
        <Slider ref={(ref) => (slider.current = ref)} {...settings}>
          <Screen0 />
          <Screen1 />
          <Screen2 />
          <Screen3 />
          <Screen4 />
          <Screen5 />
        </Slider>
        <div className="navdown">
          <a onClick={previous} href="#top-catalog" offset={50} duration={500}>
            <ArrowIcon className="arrow" />
          </a>
          <a onClick={next} href="#top-catalog" offset={50} duration={500}>
            <ArrowIcon className="arrow arrow__right" />
          </a>
        </div>
      </div>
    </div>
  );
}
