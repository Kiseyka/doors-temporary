import styles from './Button.module.scss';
const Button = ({ text, onClick, param, style, type }) => {
  return (
    <button
      onClick={() => onClick(!param)}
      className={styles.button}
      style={style}
      type={type}
    >
      {text}
    </button>
  );
};

export default Button;
