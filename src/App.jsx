import VkIcon from './components/icons/VkIcon/VkIcon';
import WhatsappIcon from './components/icons/WhatsappIcon/WhatsappIcon';
import TelegramIcon from './components/icons/TelegramIcon/TelegramIcon';
import Home from './home/page';

export let contacts = {
  phone: '+79119978257',
  adress: 'Проспект Энгельса 139/21А, ТК Шувалово, 91 секция,2 этаж',
  lat: '',
  lan: '',
  email: 'info@mebel-178.ru',
  vk: 'https://vk.com/mebel_comfort_spb',
  telegram: 'https://t.me/mebel_178',
  whatsapp: 'https://wa.me/79119978257',
};

function App() {
  return (
    <div className="container">
      <Home />
      <div className="content contacts">
        <h3 className="title">КОНТАКТЫ</h3>
        <h2 className="name">Мебель 178</h2>
        <div className="info">
          <div className="info__item">
            <div className="item__title"> ПРИЕЗЖАЙТЕ</div>
            <p className="item__text">{contacts.adress}</p>
          </div>
          <div className="info__item">
            <div className="item__title">ПИШИТЕ</div>
            <a className="item__text" href={`mailto:${contacts.email}`}>
              {contacts.email}
            </a>
          </div>
          <div className="info__item">
            <div className="item__title">ЗВОНИТЕ</div>
            <a className="item__text" href={`tel:${contacts.phone1}`}>
              {contacts.phone}
            </a>
          </div>
          <div className="info__item">
            <div className="item__title">СОЦИАЛЬНЫЕ СЕТИ</div>
            <div className="icons">
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://vk.com/mebel_comfort_spb"
              >
                <VkIcon className="icon" />
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://wa.me/79119978257"
              >
                <WhatsappIcon />
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://t.me/mebel_178"
              >
                <TelegramIcon className="icon" />
              </a>
            </div>
          </div>
        </div>

        <div className="feedback">
          <div className="feedback__info">
            <div className="text">
              <h3 className="feedback__title">РЕКВИЗИТЫ</h3>
              <p>ИП Александрова Виктория Евгеньевна</p>
              <p>ИНН 780527962236</p>
              <p>
                Тел.: <a href="tel:+79119978257"> +79119978257</a>
              </p>
              <p>
                E-mail:
                <a href="mailto:info@mebel-178.ru"> info@mebel-178.ru</a>
              </p>
              <p>
                Юр. адрес: Проспект Энгельса 139/21А, ТК Шувалово, 91 секция,2
                этаж
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
