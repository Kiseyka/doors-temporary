import intro from '../../../assets/intro.jpg';
export default function Intro() {
  return (
    <div className="intro">
      <p className="text__top">
        Производство шкафов, дверей купе и межкомнатных перегородок на заказ в
        Санкт-Петербурге.
      </p>
      <img src={intro} className="image" alt="" />
      <p className="text__bottom">
        Делаем дом красивым. Мы воплощаем мечты, придаем форму вашим идеям.
        Измените свой дом. Двери с историей.
      </p>
    </div>
  );
}
