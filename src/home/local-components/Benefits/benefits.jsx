import factoryIcon from '../../../assets/icons/factory-icon.svg';
import sketchIcon from '../../../assets/icons/sketch-icon.svg';
import installationIcon from '../../../assets/icons/installation-icon.svg';
import choiceIcon from '../../../assets/icons/choice-icon.svg';
import fastTimeIcon from '../../../assets/icons/fast-time-icon.svg';
import warrantyIcon from '../../../assets/icons/warranty-icon.svg';

export const benefits = [
  {
    icon: <img src={factoryIcon} alt="" />,
    description: 'Собственное производство. Работаем с 2017 года',
  },
  {
    icon: <img src={choiceIcon} alt="" />,
    description: 'Огромный выбор материалов',
  },
  {
    icon: <img src={sketchIcon} alt="" />,
    description: 'Выезд замерщика с образцами',
  },
  {
    icon: <img src={fastTimeIcon} alt="" />,
    description: 'Минимальные сроки производства',
  },
  {
    icon: <img src={installationIcon} alt="" />,
    description: 'Установка в день доставки',
  },
  {
    icon: <img src={warrantyIcon} alt="" />,
    description: 'Гарантия 2 года',
  },
];
