import { benefits } from './benefits.jsx';
export default function BenefitsBlock() {
  const benefitsArray = benefits.map((benefit) => (
    <div className="wrapper" key={benefit.description}>
      <div className="icons">{benefit?.icon}</div>
      <p className="item__title">{benefit?.description}</p>
    </div>
  ));
  return (
    <div className="benefits">
      <h3 className="title">НАШИ ГЛАВНЫЕ ПРЕИМУЩЕСТВА</h3>
      <div className="inner">{benefitsArray}</div>
    </div>
  );
}
