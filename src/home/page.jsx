import Intro from './local-components/Intro/page';
import BenefitsBlock from './local-components/Benefits/page';
import Catalog from '../components/catalog/page';
export default function Home() {
  return (
    <div className="container">
      <Intro />
      <div className="content">
        <BenefitsBlock />
        <Catalog />
      </div>
    </div>
  );
}
